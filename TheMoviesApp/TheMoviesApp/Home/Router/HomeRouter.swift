//
//  HomeRouter.swift
//  TheMoviesApp
//
//  Created by omar hernandez on 02/08/21.
//

import UIKit

class HomeRouter {
    var sourceView: UIViewController?
    var viewController: UIViewController {
        return crateViewController()
    }
    private func crateViewController() -> UIViewController {
        let view = HomeView(nibName: "HomeView", bundle: .main)
        return view
    }
    func setSourceView(_ sourceView: UIViewController?) {
        guard let view = sourceView else { fatalError("Error desconocido") }
        self.sourceView = view
    }
}
