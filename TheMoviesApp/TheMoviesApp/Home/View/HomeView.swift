//
//  HomeView.swift
//  TheMoviesApp
//
//  Created by omar hernandez on 02/08/21.
//

import UIKit

class HomeView: UIViewController {
    private var router = HomeRouter()
    private var viewModel = HomeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(view: self, router: router)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
