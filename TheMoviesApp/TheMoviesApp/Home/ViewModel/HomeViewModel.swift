//
//  HomeViewModel.swift
//  TheMoviesApp
//
//  Created by omar hernandez on 02/08/21.
//

import Foundation
class HomeViewModel {
    private weak var view: HomeView?
    private var router: HomeRouter?
    
    func bind(view: HomeView, router: HomeRouter) {
        self.view = view
        self.router = router
    }
    
}
